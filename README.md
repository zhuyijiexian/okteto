# 利用okteto
## 进入虚拟机的root权限
```
sudo -i
```
## **虚拟机下载**
```
apt install npm
npm install -g wstunnel
```
## 打开终端(//后面改为okteto生成网页保留&)
```
wstunnel -t 12345:127.0.0.1:22 wss://okteto_url &
```
### 打开另一个终端进行 
```
ssh root@127.0.0.1 -p 12345
```
### **密码**-可改file或连接后passwd更改-
```
799632
```
## 连接网页后下载宝塔面板进行对宝塔面板的改动
### 建立宝塔的本地映射
```
wstunnel -t 12388:127.0.0.1:8888 wss://areyouok-zhuyijiexian.cloud.okteto.net &
```
### 打开新终端下载文件更改宝塔登陆页面解压文件
提醒下载文件输入
```
ls
```
确认文件名，输入
```
bash  文件名
```
下载文件
#### 安装后进行
```
wget http://download.bt.cn/install/update/LinuxPanel-7.7.0.zip
```
ls 来显示文件列表
```    
ls
```
unzip 文件名
```
unzip
```
进入该文件
```
cd panel
```
```
ls
```
找到update.sh输入
```
bash update.sh
```
然后输入
```
rm -rf /www/server/panel/data/bind.pl
```
来移除登陆验证

## 连接后在宝塔面板上下载以及改动
下载ded文件 
```
https://github.com/cloudflare/cloudflared/releases/download/2021.11.0/cloudflared-linux-amd64.deb
```
输入 
```
dpkg -i 文件名.ded
```
关联cf账号
```
cloudflared tunnel login
```
进入链接

创建隧道：
```
cloudflared tunnel create 隧道名
```
删除隧道：
```
cloudflared tunnel delete 隧道名
```
列出隧道：
```
cloudflared tunnel list
```
配置隧道：
```
cloudflared tunnel route dns 隧道名 刚才选择的域名(选择的域名如：xxx.tmax.cf)
```
隧道指向：
```
cloudflared tunnel run --url http://127.0.0.1:8888 --no-tls-verify 隧道名称
```
